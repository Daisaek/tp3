package com.example.tp3;

import android.util.JsonReader;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 * Process the response to a GET request to the Web service
 * https://www.thesportsdb.com/api/v1/json/1/lookuptable.php?l=x&s=y
 * Responses must be provided in JSON.
 *
 */

public class JSONResponseHandlerLigue {

    private static final String TAG = JSONResponseHandlerTeam.class.getSimpleName();

    private Team team;


    public JSONResponseHandlerLigue(Team team) {
        this.team = team;
    }

    /**
     * @param response done by the Web service
     * @return A Team with attributes filled with the collected information if response was
     * successfully analyzed
     */
    public void readJsonStream(InputStream response) throws IOException {
        JsonReader reader = new JsonReader(new InputStreamReader(response, "UTF-8"));
        try {
            readClass(reader);
        } finally {
            reader.close();
        }
    }

    public void readClass(JsonReader reader) throws IOException {
        reader.beginObject();
        while (reader.hasNext()) {
            String name = reader.nextName();
            if (name.equals("table")) {
                readArrayClass(reader);
            } else {
                reader.skipValue();
            }
        }
        reader.endObject();
    }

    private void readArrayClass(JsonReader reader) throws IOException {
        reader.beginArray();
        int nb = 0;
        boolean find = false;
        while (reader.hasNext() ) {
            reader.beginObject();
            while (reader.hasNext()) {
                String name = reader.nextName();
                if (name.equals("teamid")) {
                    if (reader.nextLong() == team.getId()) {
                        team.setRanking(nb+1);
                        find = true;
                    }
                }
                else if(name.equals("total")){
                    if(find == true){
                        team.setTotalPoints(reader.nextInt());
                    }
                }
                else{
                    reader.skipValue();
                }
            }
            reader.endObject();
            nb++;
        }
        reader.endArray();
    }
}
