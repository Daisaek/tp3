package com.example.tp3;

import android.util.JsonReader;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 * Process the response to a GET request to the Web service
 * https://www.thesportsdb.com/api/v1/json/1/eventslast.php?id=z
 * Responses must be provided in JSON.
 *
 */

public class JSONResponseHandlerMatch {

    private static final String TAG = JSONResponseHandlerTeam.class.getSimpleName();

    private Team team;


    public JSONResponseHandlerMatch(Team team) {
        this.team = team;
    }

    /**
     * @param response done by the Web service
     * @return A Team with attributes filled with the collected information if response was
     * successfully analyzed
     */
    public void readJsonStream(InputStream response) throws IOException {
        JsonReader reader = new JsonReader(new InputStreamReader(response, "UTF-8"));
        try {
            readMatch(reader);
        } finally {
            reader.close();
        }
    }

    public void readMatch(JsonReader reader) throws IOException {
        reader.beginObject();
        while (reader.hasNext()) {
            String name = reader.nextName();
            if (name.equals("results")) {
                readArrayMatch(reader);
            } else {
                reader.skipValue();
            }
        }
        reader.endObject();
    }


    private void readArrayMatch(JsonReader reader) throws IOException {
        reader.beginArray();
        int nb = 0;
        long idMatch = 0;
        String label = null;
        String homeTeam = null;
        String awayTeam = null;
        int homeScore = 0;
        int awayScore = 0;
        while (reader.hasNext()) {
            reader.beginObject();
            while (reader.hasNext()) {
                String name = reader.nextName();
                if (nb == 0) {
                    if (name.equals("idEvent")) {
                        idMatch = reader.nextLong();
                    } else if (name.equals("strEvent") && reader.nextString() != null) {
                        label = reader.nextString();
                    } else if (name.equals("strHomeTeam") && reader.nextString() != null) {
                        homeTeam = reader.nextString();
                    } else if (name.equals("strAwayTeam") && reader.nextString() != null) {
                        awayTeam = reader.nextString();
                    } else if (name.equals("strHomeScore") && reader.nextString() != null) {
                        homeScore = reader.nextInt();
                    } else if (name.equals("strAwayScore") && reader.nextString() != null) {
                        awayScore = reader.nextInt();
                    }
                    else{
                        reader.skipValue();
                    }
                }
                else{
                    reader.skipValue();
                }
            }
            reader.endObject();
            nb++;
        }
        reader.endArray();
        Match m = new Match(idMatch, label, homeTeam, awayTeam, homeScore, awayScore);
        team.setLastEvent(m);
    }

}
