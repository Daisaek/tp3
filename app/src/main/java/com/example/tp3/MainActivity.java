package com.example.tp3;

import android.content.Intent;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Collections;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    protected static SportDbHelper dbHelper;
    SimpleCursorAdapter ADAPTER;
    SwipeRefreshLayout SWIPE;
    ListView TEAM;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        SWIPE = findViewById(R.id.swipe);
        TEAM = findViewById(R.id.listteam);
        dbHelper = new SportDbHelper(this);
        dbHelper.populate();
        ADAPTER = new SimpleCursorAdapter(this, android.R.layout.simple_list_item_2,
                dbHelper.fetchAllTeams(), new String[] { SportDbHelper.COLUMN_TEAM_NAME, SportDbHelper.COLUMN_LEAGUE_NAME },
                new int[] { android.R.id.text1, android.R.id.text2});
        TEAM.setAdapter(ADAPTER);

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent newTeam = new Intent(MainActivity.this, NewTeamActivity.class);
                newTeam.putExtra("NEWTEAM_CLICK", new Team());
                startActivity(newTeam);
            }
        });

        TEAM.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(MainActivity.this, TeamActivity.class);
                intent.putExtra("TEAM_CLICK", dbHelper.cursorToTeam((Cursor)parent.getItemAtPosition(position)));
                startActivity(intent);
            }
        });

        SWIPE.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                // cancle the Visual indication of a refresh
                SWIPE.setRefreshing(false);
                List<Team> teams = dbHelper.getAllTeams();
                new MajAllTeam().execute(teams);
            }
        });
    }

    class MajAllTeam extends AsyncTask<List<Team>, Integer, Long>{
        private List<Team> allTeams;
        @Override
        protected Long doInBackground(List<Team>... teams) {
            allTeams = teams[0];
            for(Team team : allTeams) {
                URL urlTeam = null;
                URL urlRank = null;
                URL urlMatch = null;
                try {
                    urlTeam = WebServiceUrl.buildSearchTeam(team.getName());
                    urlRank = WebServiceUrl.buildGetRanking(team.getIdLeague());
                    urlMatch = WebServiceUrl.buildSearchLastEvents(team.getIdTeam());
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                }
                HttpURLConnection urlConnectionTeam = null;
                HttpURLConnection urlConnectionRank = null;
                HttpURLConnection urlConnectionMatch = null;
                try {
                    urlConnectionTeam = (HttpURLConnection) urlTeam.openConnection();
                    urlConnectionRank = (HttpURLConnection) urlRank.openConnection();
                    urlConnectionMatch = (HttpURLConnection) urlMatch.openConnection();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                try {
                    InputStream inTeam = new BufferedInputStream(urlConnectionTeam.getInputStream());
                    JSONResponseHandlerTeam JTeam = new JSONResponseHandlerTeam(team);
                    JTeam.readJsonStream(inTeam);
                    InputStream inRank = new BufferedInputStream(urlConnectionRank.getInputStream());
                    JSONResponseHandlerLigue JRank = new JSONResponseHandlerLigue(team);
                    JRank.readJsonStream(inRank);
                    InputStream inMatch = new BufferedInputStream(urlConnectionMatch.getInputStream());
                    JSONResponseHandlerMatch JMatch = new JSONResponseHandlerMatch(team);
                    JMatch.readJsonStream(inMatch);
                } catch (IOException e) {
                    e.printStackTrace();
                } finally {
                    urlConnectionTeam.disconnect();
                    urlConnectionRank.disconnect();
                    urlConnectionMatch.disconnect();
                }
            }
            Long n = 100L;
            return n;
        }
        @Override
        protected void onProgressUpdate(Integer... prog){

        }
        @Override
        protected void onPostExecute(Long result) {
            //ADAPTER = new SimpleCursorAdapter(this, android.R.layout.simple_list_item_2,
            //        dbHelper.fetchAllTeams(), new String[] { SportDbHelper.COLUMN_TEAM_NAME, SportDbHelper.COLUMN_LEAGUE_NAME },
            //        new int[] { android.R.id.text1, android.R.id.text2});
            //TEAM.setAdapter(ADAPTER);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
