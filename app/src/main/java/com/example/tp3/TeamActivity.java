package com.example.tp3;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.lang.Long;

import com.example.tp3.Match;
import com.example.tp3.Team;
import com.example.tp3.WebServiceUrl;

public class TeamActivity extends AppCompatActivity {

    private static final String TAG = TeamActivity.class.getSimpleName();
    private TextView textTeamName, textLeague, textManager, textStadium, textStadiumLocation, textTotalScore, textRanking, textLastMatch, textLastUpdate;


    private int totalPoints;
    private int ranking;
    private Match lastEvent;
    private String lastUpdate;

    private ImageView imageBadge;
    private Team team;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_team);

        team = (Team) getIntent().getParcelableExtra("TEAM_CLICK");

        textTeamName = (TextView) findViewById(R.id.nameTeam);
        textLeague = (TextView) findViewById(R.id.league);
        textStadium = (TextView) findViewById(R.id.editStadium);
        textStadiumLocation = (TextView) findViewById(R.id.editStadiumLocation);
        textTotalScore = (TextView) findViewById(R.id.editTotalScore);
        textRanking = (TextView) findViewById(R.id.editRanking);
        textLastMatch = (TextView) findViewById(R.id.editLastMatch);
        textLastUpdate = (TextView) findViewById(R.id.editLastUpdate);

        imageBadge = (ImageView) findViewById(R.id.imageView);

        updateView();

        final Button but = (Button) findViewById(R.id.button);

        but.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                new MajTeamTask().execute(team);

            }
        });

    }

    @Override
    public void onBackPressed() {
        //TODO : prepare result for the main activity
        super.onBackPressed();
    }

    private void updateView() {

        textTeamName.setText(team.getName());
        textLeague.setText(team.getLeague());
        textStadium.setText(team.getStadium());
        textStadiumLocation.setText(team.getStadiumLocation());
        textTotalScore.setText(Integer.toString(team.getTotalPoints()));
        textRanking.setText(Integer.toString(team.getRanking()));
        textLastMatch.setText(team.getLastEvent().toString());
        textLastUpdate.setText(team.getLastUpdate());

	//TODO : update imageBadge
    }

    class MajTeamTask extends AsyncTask<Team, Void, Long> {
        private Team team;
        @Override
        protected Long doInBackground(Team... teams) {
            team = teams[0];
            URL urlTeam = null;
            URL urlRank = null;
            URL urlMatch = null;
            try {
                urlTeam = WebServiceUrl.buildSearchTeam(team.getName());
                urlRank = WebServiceUrl.buildGetRanking(team.getIdLeague());
                urlMatch = WebServiceUrl.buildSearchLastEvents(team.getIdTeam());
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
            HttpURLConnection urlConnectionTeam = null;
            HttpURLConnection urlConnectionRank = null;
            HttpURLConnection urlConnectionMatch = null;
            try {
                urlConnectionTeam = (HttpURLConnection) urlTeam.openConnection();
                urlConnectionRank = (HttpURLConnection) urlRank.openConnection();
                urlConnectionMatch = (HttpURLConnection) urlMatch.openConnection();
            } catch (IOException e) {
                e.printStackTrace();
            }
            try{
                InputStream inTeam = new BufferedInputStream(urlConnectionTeam.getInputStream());
                JSONResponseHandlerTeam JTeam = new JSONResponseHandlerTeam(team);
                JTeam.readJsonStream(inTeam);
                InputStream inRank = new BufferedInputStream(urlConnectionRank.getInputStream());
                JSONResponseHandlerLigue JRank = new JSONResponseHandlerLigue(team);
                JRank.readJsonStream(inRank);
                InputStream inMatch = new BufferedInputStream(urlConnectionMatch.getInputStream());
                JSONResponseHandlerMatch JMatch = new JSONResponseHandlerMatch(team);
                JMatch.readJsonStream(inMatch);
            } catch (IOException e) {
                e.printStackTrace();
            } finally{
                urlConnectionTeam.disconnect();
                urlConnectionRank.disconnect();
                urlConnectionMatch.disconnect();
            }
            Long n = 100L;
            return n;
        }
        @Override
        protected void onPostExecute(Long result) {
            updateView();
        }
    }

}
